import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import { Navbar } from './core/components/Navbar';
import { PrivateRoute } from './core/auth/PrivateRoute';

import { PageHome } from './features/PageHome';
import { PageAdmin } from './features/PageAdmin';
import { PageLogin } from './features/PageLogin';
import { PageSettings } from './features/PageSettings';

const App: React.FC = () => {
  return (
    <Router>
      <div>

        <Navbar />

        <Switch>
          <Route path="/home">
            <PageHome />
          </Route>
          <Route path="/login">
            <PageLogin />
          </Route>
          <PrivateRoute path="/settings">
            <PageSettings />
          </PrivateRoute>
          <PrivateRoute path="/admin">
            <PageAdmin />
          </PrivateRoute>
          <Route path="*">
            <PageLogin />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
